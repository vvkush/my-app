import React from "react";

export const Hello = (props) => <h1>Hello, {props.name}</h1>;