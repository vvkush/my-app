import React from "react";
import { Hello } from "./hello";

export class HelloContainer extends React.Component {
    render() {
        return <Hello name="world"/>
    }
}