import React from "react";
import logo from '../logo.svg';

export class Logo extends React.Component {
    render() {
        return <img src={logo} className="App-logo" alt="logo" />
    }
}