import React from "react";
import './App.css';
import { HelloContainer } from "./components/helloContainer";
import { Logo } from "./components/logo";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Logo></Logo>
        { React.createElement(HelloContainer) }
      </header>
    </div>
  );
}

export default App;
